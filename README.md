# Challenges

Source for programming challenges I participated.

Mainly, I try to use C/C++, but some of these challenges are for testing my
expertise on other programming languages, specially those I'm not very experient
at.

Not all of them are finished nor correct, but most of the things can be
interesting.

## Various

Various interesting challenges and stuff.

- binary_mult
- Erlang
- LFE
- ies
- PCL
- rpncalculator
- problem-20180929.c

### rpncalculator

Simple Reverse Polish Notation calculator, written in F#.

### iec

An incremental economy simulator/game, written in Common Lisp.

## SPOJ

SPOJ Brasil challenges.

## URI

URI Online Judge challenges.
To-do:
- Finish 1408
- 1229
- 1093
- 1252
- 1237

## 99LispProblems

Solution to the 99 Lisp Problems at http://www.ic.unicamp.br/~meidanis/courses/mc336/2006s2/funcional/L-99_Ninety-Nine_Lisp_Problems.html.

## NeuralNetworkJS

Awesome code showing how to build a small and quick neural network using Synaptic.JS.

## ProjectEuler

Codes and execution for getting Project Euler solutions.

## maratona-2018-1

In the first semester of 2018, I attended to a programming contest which I won 1st place, however I didn't finish all the problems. for that reason, I decided to finish them at home, and then uploaded them on this folder.

The actual description of the problems is missing, so bear with me. Includes its own readme (in Portuguese) and license (MIT).

