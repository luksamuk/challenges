;;; https://osprogramadores.com/desafios/d05/

(ql:quickload :cl-json)

(defpackage #:desafio5
  (:use #:cl #:cl-json)
  (:export :*db*
	   :funcionario-salario
	   :funcionario-nome
	   :load-json-file
	   :print-salary-globals))

;; ========================

(in-package #:desafio5)

(defparameter *db* nil)

(defun load-json-file (file-name)
  (with-open-file (stream file-name)
    (setf *db* (cl-json:decode-json stream))))

(defmacro funcionario-salario (funcionario)
  `(cdr (nth 3 ,funcionario)))

(defmacro funcionario-nome (funcionario)
  `(concatenate 'string
		(cdr (nth 1 ,funcionario))
		" "
		(cdr (nth 2 ,funcionario))))

(defun print-salary-globals ()
  (when *db*
    (let* ((funcionarios (cdar *db*))
	   (max (car funcionarios))
	   (min (car funcionarios))
	   (avg 0))
      ;; Naive implementation
      (loop for funcionario in funcionarios
	 do (let ((salario (funcionario-salario funcionario)))
	      (incf avg salario)
	      (cond ((> salario (funcionario-salario max))
		     (setf max funcionario))
		    ((< salario (funcionario-salario min))
		     (setf min funcionario)))))
      (format t "global_max|~a|~a~%global_min|~a|~a~%global_avg|~a~&"
	      (funcionario-nome max) (funcionario-salario max)
	      (funcionario-nome min) (funcionario-salario min)
	      (/ avg (list-length funcionarios))))))
